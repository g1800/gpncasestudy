Feature: Search and Filter Feature

Scenario Outline: Search scenario and Filter Products
Given user is on Neutrogena page
When fills the search bar using sheetname "<SheetName>" and rownumber <RowNumber>
And user clicks on search button
And user filter checkbox
And user clicks on product link
And user navigates to product buy page


Examples:
|SheetName|RowNumber|
|filter|0|