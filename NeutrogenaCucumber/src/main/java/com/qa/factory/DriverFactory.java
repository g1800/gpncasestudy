package com.qa.factory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class DriverFactory {

	public WebDriver driver;
	public static ThreadLocal<WebDriver> tlDriver = new ThreadLocal<>();

	public WebDriver init_driver(String browser) {
		System.out.println("broswer value is :" + browser);
		if (browser.equals("chrome")) {
			WebDriverManager.chromedriver().setup(); //setting as Chromedriver
			tlDriver.set(new ChromeDriver()); 
		} else {
			System.out.println("Please pass the correct browser value: " + browser);
		}
		getDriver().manage().deleteAllCookies(); //allows to detete all coookies after browser automation.
		getDriver().manage().window().maximize(); // allows to maximise the windows for automation!
		return getDriver();
	}

	//Synchronised is used because multiple threads/pages will be accessing this
	public static synchronized WebDriver getDriver() {
		return tlDriver.get(); //if someone calls getDriver it will return Threadlocal driver after checking if else condition.
	}
}
