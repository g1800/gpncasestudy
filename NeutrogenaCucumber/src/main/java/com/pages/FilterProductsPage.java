package com.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class FilterProductsPage {

	private WebDriver driver;
	// By Locators::
	private By searchbar = By.xpath("//*[@id=\"edit-apachesolr-panels-search-form\"]");
	private By searchButton = By.xpath("//*[@id=\"edit-submit\"]");
	private By filterCheck = By.xpath("//a[@id=\"facetapi-link--5\"]");

	// Constructor of the page class.

	public FilterProductsPage(WebDriver driver) {
		this.driver = driver;
	}
	// Page Actions:features of page in the form of methods.

	public boolean logoCheck() {
		boolean flag = driver.findElement(By.xpath("//img[@alt='Home']")).isDisplayed();
		return flag;

	}

	public void fillSearchBar(String searchproduct) {

		driver.findElement(searchbar).sendKeys(searchproduct);

	}

	public void clickSearch() {
		driver.findElement(searchButton).click();
	}

	public void filterClick() {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElement(filterCheck).click();
	}

	public WebElement getProduct() {
		return driver.findElement(By.linkText("Neutrogena® Ultra Sheer Dry Touch Sunscreen SPF 50+"));
	}

	public void clicksonProduct() {
		getProduct().click();
	}

	public String productBuyPage() {
		return driver.getCurrentUrl();

	}

}
