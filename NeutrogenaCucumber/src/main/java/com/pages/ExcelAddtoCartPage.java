package com.pages;

import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ExcelAddtoCartPage {
	private WebDriver driver;
	private By searchbar = By.xpath("//*[@id=\"edit-apachesolr-panels-search-form\"]");
	private By searchButton = By.xpath("//*[@id=\"edit-submit\"]");

	public ExcelAddtoCartPage(WebDriver driver) {
		this.driver = driver;
	}

	public boolean logoCheck() {
		boolean flag = driver.findElement(By.xpath("//img[@alt='Home']")).isDisplayed();
		return flag;

	}

	public void fillSearchBar(String searchproduct) {

		driver.findElement(searchbar).sendKeys(searchproduct);

	}

	public void clickSearch() {
		driver.findElement(searchButton).click();
	}

	public WebElement getNeutrogenaProduct() {
		return driver.findElement(
				By.xpath("//*[@id=\"main-content\"]/div/div[1]/div[2]/div[2]/div[3]/article[3]/div/div[2]/h2/a"));
	}

	public void clicksonProduct() {
		getNeutrogenaProduct().click();
	}

	public WebElement getProductBuyButton() {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.switchTo().frame(driver.findElement(By.xpath("//*[@id=\"where-to-buy\"]/li/div/iframe")));
		return driver.findElement(By.xpath("//*[@id=\"Nykaa.inBuyNow\"]/a"));
	}

	public void clicksonNyka() {
		getProductBuyButton().click();
	}

	public void addtoCart() {
		String currentWindow = driver.getWindowHandle();// get handle of current window
		Set<String> handles = driver.getWindowHandles();// get handle of all windows
		Iterator<String> it = handles.iterator();
		while (it.hasNext()) {
			if (currentWindow == it.next()) {
				continue;
			}
			driver = driver.switchTo().window(it.next());
			driver.findElement(By.xpath("//span[@class=\"btn-text\"]")).click();
		}
	}

	public void getCartButton() {
		String currentWindow = driver.getWindowHandle();// get handle of current window
		Set<String> handles = driver.getWindowHandles();// get handle of all windows
		Iterator<String> it = handles.iterator();
		while (it.hasNext()) {
			if (currentWindow == it.next()) {
				continue;
			}
			driver = driver.switchTo().window(it.next());
			driver.findElement(By.xpath("//*[@id=\"header_id\"]/div[2]/div/div[2]/div[2]/button")).click();
		}

	}

	public void clicksonCartButton() {
		getCartButton();
	}

	public String navCartPage() {
		return driver.getCurrentUrl();

	}

}
