package com.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class SkinCarePage {
	private WebDriver driver;

	public SkinCarePage(WebDriver driver) {
		this.driver = driver;
	}

	public WebElement getNeutrogenaSkinCareTips() {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		return driver.findElement(By.xpath("//*[@id=\"content\"]/header/div/div/div/div[3]/div/div/div/nav/ul/li[5]/div/div[1]/div/div/div/div/a"));

	}

	public void clickonNeutrogenaSkinCareTips() {
		getNeutrogenaSkinCareTips().click();
	}

	public String navSkinCareTipsPage() {
		return driver.getCurrentUrl();

	}

	public WebElement getProductReadMore() {
		return driver.findElement(
				By.xpath("//*[@id=\"main-content\"]/div/div[1]/div[2]/div/div[2]/div/div[2]/div[1]/div/div[2]"));

	}

	public void clickonProductReadMore() {
		getProductReadMore().click();
	}

	public String itemExist() {
		String name = driver.findElement(By.xpath("//h1[@id='content-main']")).getText();

		return name;

	}

}
