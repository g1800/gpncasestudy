package com.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ExcelSearchPage {
	private WebDriver driver;
	private By searchbar = By.xpath("//*[@id=\"edit-apachesolr-panels-search-form\"]");
	private By searchButton = By.xpath("//*[@id=\"edit-submit\"]");

	public ExcelSearchPage(WebDriver driver) {
		this.driver = driver;
	}

	public boolean logoCheck() {
		boolean flag = driver.findElement(By.xpath("//img[@alt='Home']")).isDisplayed();
		return flag;

	}

	public void fillSearchBar(String searchproduct) {

		driver.findElement(searchbar).sendKeys(searchproduct);

	}

	public void clickSearch() {
		driver.findElement(searchButton).click();
	}

	public String navProductsPage() {
		return driver.getCurrentUrl();

	}

}
