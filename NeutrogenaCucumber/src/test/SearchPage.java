package com.pages;

import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class SearchPage {

	private WebDriver driver;

	public SearchPage(WebDriver driver) {
		this.driver = driver;
	}

	public WebElement getNeutrogenaKey() {
		return driver.findElement(By.xpath("//*[@id=\"edit-apachesolr-panels-search-form\"]"));

	}

	public void enterProductName(String neutrokey) {
		getNeutrogenaKey().sendKeys(neutrokey);
	}

	public WebElement getNeutrogenaSearchButton() {
		return driver.findElement(By.xpath("//*[@id=\"edit-submit\"]"));

	}

	public void clicksonSearch() {
		getNeutrogenaSearchButton().click();
	}

	public WebElement getNeutrogenaProduct() {
		return driver.findElement(
				By.xpath("//*[@id=\"main-content\"]/div/div[1]/div[2]/div[2]/div[3]/article[1]/div/div[2]/h2/a"));
	}

	public void clicksonProduct() {
		getNeutrogenaProduct().click();
	}

	public WebElement getProductBuyButton() {
		return driver.findElement(
				By.xpath("//*[@id=\"exclusively-available-nykaa\"]/li/div/div/div/div/div/div/div/div/div/div/a/img"));
	}

	public void clicksonNyka() {
		getProductBuyButton().click();
	}

	public void addtoCart() {
		String currentWindow = driver.getWindowHandle();// get handle of current window
		Set<String> handles = driver.getWindowHandles();// get handle of all windows
		Iterator<String> it = handles.iterator();
		while (it.hasNext()) {
			if (currentWindow == it.next()) {
				continue;
			}
			driver = driver.switchTo().window(it.next());
			driver.findElement(By.xpath("//span[@class=\"btn-text\"]")).click();
		}
	}

	public void getCartButton() {
		String currentWindow = driver.getWindowHandle();// get handle of current window
		Set<String> handles = driver.getWindowHandles();// get handle of all windows
		Iterator<String> it = handles.iterator();
		while (it.hasNext()) {
			if (currentWindow == it.next()) {
				continue;
			}
			driver = driver.switchTo().window(it.next());
			driver.findElement(By.xpath("//*[@id=\"header_id\"]/div[2]/div/div[2]/div[2]/button")).click();
		}

	}

	public void clicksonCartButton() {
		getCartButton();
	}

	public String navCartPage() {
		return driver.getCurrentUrl();

	}
}
