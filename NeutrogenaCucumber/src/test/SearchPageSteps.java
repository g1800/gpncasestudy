package stepdefinitions;

import org.openqa.selenium.WebDriver;
import static org.junit.Assert.assertEquals;

import com.pages.SearchPage;
import com.qa.factory.DriverFactory;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import org.junit.Assert;

public class SearchPageSteps {
	WebDriver driver = null;
	private SearchPage searchPage = new SearchPage(DriverFactory.getDriver());

	@Given("Neutrogena home page for user")
	public void neutrogena_home_page_for_user() {
		DriverFactory.getDriver().get("https://www.neutrogena.in/");
	}

	@When("user enters the product name {string}")
	public void user_enters_the_product_name(String neutrokey) {
		searchPage.enterProductName(neutrokey);
	}

	@When("user clicks Neutrogena search button")
	public void user_clicks_neutrogena_search_button() {
		searchPage.clicksonSearch();
	}

	@When("user clicks on product")
	public void user_clicks_on_product() {
		searchPage.clicksonProduct();
	}

	@When("user clicks on buy now button of {string}")
	public void user_clicks_on_buy_now_button_of(String string) {
		searchPage.clicksonNyka();
	}

	@When("user add product to cart")
	public void user_add_product_to_cart() {
		searchPage.addtoCart();

	}

	@When("user clicks on cart")
	public void user_clicks_on_cart() {
		searchPage.clicksonCartButton();
	}

	@Then("should navigate to cart page")
	public void should_navigate_to_cart_page() {

		String url = searchPage.navCartPage();
		assertEquals(url,
				"https://www.nykaa.com/neutrogena-bright-boost-illuminating-serum/p/5290323?productId=5290323&pps=2");

	}

}
