Feature: Search Feature

Scenario Outline: Search scenario and AddtoCart
Given user is on Neutrogena homepage
When fills the search bar from given sheetname "<SheetName>" and rownumber <RowNumber>
And user clicks on serach button
And user clicks on product
And user clicks on buy now button
And user add product to cart 
And user clicks on cart
Then should navigate to cart page

Examples:
|SheetName|RowNumber|
|search|0|
