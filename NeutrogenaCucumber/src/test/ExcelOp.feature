Feature: Search Feature

Scenario Outline: Search scenario using different set of data
Given user is on Neutrogena homepage
When fills the search bar from given sheetname "<SheetName>" and rownumber <RowNumber>
And user clicks on serach button
Then it navigates to product page

Examples:
|SheetName|RowNumber|
|search|0|
|search|1|
