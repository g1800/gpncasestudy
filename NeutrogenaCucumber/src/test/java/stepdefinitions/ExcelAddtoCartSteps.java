package stepdefinitions;

import static org.junit.Assert.assertEquals;

import java.util.List;
import java.util.Map;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.junit.Assert;

import com.pages.ExcelAddtoCartPage;

import com.qa.factory.DriverFactory;
import com.qa.util.ExcelReader;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class ExcelAddtoCartSteps {
	private ExcelAddtoCartPage exceladdtoCart = new ExcelAddtoCartPage(DriverFactory.getDriver());

	@Given("user is on Neutrogena homepage")
	public void user_is_on_neutrogena_homepage() {
		DriverFactory.getDriver().get("https://www.neutrogena.in/");
		boolean flag = exceladdtoCart.logoCheck();
		Assert.assertTrue(flag);
	}

	@When("fills the search bar from given sheetname {string} and rownumber {int}")
	public void fills_the_search_bar_from_given_sheetname_and_rownumber(String sheetName, Integer rowNumber)
			throws InvalidFormatException, Throwable {
		ExcelReader reader = new ExcelReader(); // Object created for ExcelReader
		List<Map<String, String>> testData = reader.getData("C:/Users/Arjun VP/Downloads/excelsearch.xlsx", sheetName); // Use
																														// getDta
																														// method
																														// from
																														// ExcelReader
		String searchbar = testData.get(rowNumber).get("search"); // getting rownumber and get the column name this case
																	// Neutrogena Bright Boost
		exceladdtoCart.fillSearchBar(searchbar);
	}

	@When("user clicks on serach button")
	public void user_clicks_on_serach_button() {
		exceladdtoCart.clickSearch();
	}

	@When("user clicks on product")
	public void user_clicks_on_product() {
		exceladdtoCart.clicksonProduct();
	}

	@When("user clicks on buy now button")
	public void user_clicks_on_buy_now_button() {
		exceladdtoCart.clicksonNyka();
	}

	@When("user add product to cart")
	public void user_add_product_to_cart() {
		exceladdtoCart.addtoCart();

	}

	@When("user clicks on cart")
	public void user_clicks_on_cart() {
		exceladdtoCart.clicksonCartButton();
	}

	@Then("should navigate to cart page")
	public void should_navigate_to_cart_page() {

		String url = exceladdtoCart.navCartPage();
		assertEquals(url, "https://www.nykaa.com/neutrogena-bright-boost-illuminating-serum/p/5290323");

	}

}
