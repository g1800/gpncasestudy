package stepdefinitions;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.junit.Assert;

import com.pages.FilterProductsPage;
import com.qa.factory.DriverFactory;
import com.qa.util.ExcelReader;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;

public class FilterProductsSteps {
	private FilterProductsPage filterProduct = new FilterProductsPage(DriverFactory.getDriver());

	@Given("user is on Neutrogena page")
	public void user_is_on_neutrogena_page() {
		DriverFactory.getDriver().get("https://www.neutrogena.in/");
		boolean flag = filterProduct.logoCheck();
		Assert.assertTrue(flag);

	}

	@When("fills the search bar using sheetname {string} and rownumber {int}")
	public void fills_the_search_bar_using_sheetname_and_rownumber(String sheetName, Integer rowNumber)
			throws InvalidFormatException, IOException {
		ExcelReader reader = new ExcelReader();
		List<Map<String, String>> testData = reader.getData("C:/Users/Arjun VP/Downloads/excelfilter.xlsx", sheetName);
		String searchbar = testData.get(rowNumber).get("search");
		filterProduct.fillSearchBar(searchbar);
	}

	@When("user clicks on search button")
	public void user_clicks_on_search_button() {

		filterProduct.clickSearch();
	}

	@When("user filter checkbox")
	public void user_filter_checkbox() {
		filterProduct.filterClick();
	}

	@When("user clicks on product link")
	public void user_clicks_on_product_link() {
		filterProduct.clicksonProduct();
	}

	@When("user navigates to product buy page")
	public void user_navigates_to_product_buy_page() {
		String url = filterProduct.productBuyPage();
		assertEquals(url,
				"https://www.neutrogena.in/sun/ultra-sheer-sunscreen");
	}

}
