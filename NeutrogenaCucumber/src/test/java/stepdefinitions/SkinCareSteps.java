package stepdefinitions;

import static org.testng.Assert.assertEquals;

import org.junit.Assert;

import com.pages.SkinCarePage;
import com.qa.factory.DriverFactory;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class SkinCareSteps {
	private SkinCarePage skincarePage = new SkinCarePage(DriverFactory.getDriver());

	@Given("User is on Neutrogena Home Page")
	public void user_is_on_neutrogena_home_page() {
		DriverFactory.getDriver().get("https://www.neutrogena.in/");
	}

	@When("User clicks on SKINCARE TIPS")
	public void user_clicks_on() {
		skincarePage.clickonNeutrogenaSkinCareTips();
	}

	@When("Navigates to SkinCare Page")
	public void navigates_to_skin_care_page() {
		String page = skincarePage.navSkinCareTipsPage();
		assertEquals(page, "https://www.neutrogena.in/skincare-tips/face");

	}

	@When("User clicks on Product Read More")
	public void user_clicks_on_product_read_more() throws InterruptedException {
		Thread.sleep(5000);
		skincarePage.clickonProductReadMore();

	}

	@When("Navigates Product Read More Page")
	public void navigates_product_read_more_page() {
		String page = skincarePage.navSkinCareTipsPage();
		assertEquals(page, "https://www.neutrogena.in/skincare-tips/face/boost-skin-hydration");

	}

	@Then("Should exist Item name")
	public void should_exist_item_name() {
		String name = skincarePage.itemExist();

		Assert.assertEquals(name, "Boost Skin Hydration: Supple skin with Neutrogena® Hydro Boost™");
	}

}
