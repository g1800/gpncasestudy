package testrunners;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(features = { "src/test/resources/AppFeatures" }, glue = { "stepdefinitions", "AppHooks" }, plugin = {
		"pretty", "json:target/MyReports/report.json", "junit:target/MyReports/report.xml",
		"com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:" }, publish = true)

public class MyTestRunner {

}
