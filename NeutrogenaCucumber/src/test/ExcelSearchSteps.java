package stepdefinitions;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.junit.Assert;

import com.pages.ExcelSearchPage;
import com.qa.factory.DriverFactory;
import com.qa.util.ExcelReader;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class ExcelSearchSteps {
	private ExcelSearchPage excelsearchPage = new ExcelSearchPage(DriverFactory.getDriver());

	@Given("user is on Neutrogena homepage")
	public void user_is_on_neutrogena_homepage() {
		DriverFactory.getDriver().get("https://www.neutrogena.in/");
		boolean flag = excelsearchPage.logoCheck();
		Assert.assertTrue(flag);
	}

	@When("fills the search bar from given sheetname {string} and rownumber {int}")
	public void fills_the_search_bar_from_given_sheetname_and_rownumber(String sheetName, Integer rowNumber)
			throws InvalidFormatException, Throwable {
		ExcelReader reader = new ExcelReader();
		List<Map<String, String>> testData = reader.getData("C:/Users/Arjun VP/Downloads/test.xlsx", sheetName);
		String searchbar = testData.get(rowNumber).get("search");
		excelsearchPage.fillSearchBar(searchbar);
	}

	@When("user clicks on serach button")
	public void user_clicks_on_serach_button() {
		excelsearchPage.clickSearch();
	}

	@Then("it navigates to product page")
	public void it_navigates_to_product_page() {
		excelsearchPage.navProductsPage();

	}

}
