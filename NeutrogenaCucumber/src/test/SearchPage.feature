Feature: Testing Neutrogena Website

Scenario: Search with product name and add to cart 
	Given Neutrogena home page for user
	When user enters the product name "Neutrogena Bright Boost"  
	And user clicks Neutrogena search button
	And user clicks on product
	And user clicks on buy now button of "Nyka"
	And user add product to cart 
	And user clicks on cart
	Then should navigate to cart page