package DataDriven;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;

import java.io.IOException;

import org.json.simple.JSONObject;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import Util.ExcelUtils;
import io.restassured.http.ContentType;

public class DataDriven {

	@DataProvider(name = "DataforPost")
	public Object[][] dataforPost() {
		// Returning an object which is two dimensional. Row and Column.
		// Object can take any data type, such a a way we can remove type casting.
		return new Object[][] { { "Graham", "Bell", 1 }, { "Dennis", "Ford", 3 } };
	}

	@DataProvider(name = "DataExcel")
	public String[][] dataforexcel() throws IOException {

		// Read data from excel.
		String path = System.getProperty("user.dir") + "/data/data.xlsx";

		int rownum = XLUtility.getRowCount(path, "Sheet1"); // Gets the row number.
		int colcount = XLUtility.getCellCount(path, "Sheet1", 1);// Gets the column count.

		// Forloops to increment both Rows and columns.
		String dataforexcel[][] = new String[rownum][colcount];

		for (int i = 1; i <= rownum; i++) {
			for (int j = 0; j < colcount; j++) {
				dataforexcel[i - 1][j] = XLUtility.getCellData(path, "Sheet1", i, j); // i-1 is because we avoiding
																						// header in Excel sheet.
			}
		}
		return (dataforexcel);

	}

	@DataProvider(name = "DataforPatch")
	public Object[][] dataforPatch() {
		return new Object[][] { { "Norton" } };
	}

	@DataProvider(name = "DataforPut")
	public Object[][] dataforPut() {
		return new Object[][] { { "Albert", "Einstein", 2 } };
	}

	@DataProvider(name = "DeleteData")
	public Object[] dataforDelete() {
		return new Object[] { 3, 4, 5 };
	}

}
