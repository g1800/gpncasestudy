package Util;

import java.io.IOException;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelUtils {
	// Constructor
	static XSSFWorkbook workbook;
	static XSSFSheet sheet;

	public ExcelUtils(String excelPath, String sheetName) throws IOException {
		workbook = new XSSFWorkbook(excelPath);
		sheet = workbook.getSheet(sheetName);

	}

	public static String getCellData(int rowNum, int colNum) throws IOException {
		DataFormatter formatter = new DataFormatter();
		String value = formatter.formatCellValue(sheet.getRow(rowNum).getCell(colNum));
		System.out.println(value);
		return value;
	}

	public static int getRowCount() throws IOException {

		int rowCount = sheet.getPhysicalNumberOfRows();
		System.out.println("No of Rows" + rowCount);
		return getRowCount();
	}

}
