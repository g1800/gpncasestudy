package RestAssuredBasicOps;

import org.testng.Assert;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.*; //Static is used so that we can Rest Assured Library methods without creating objects.
import io.restassured.response.Response;
import static org.hamcrest.Matchers.*; //.body working

public class BasicOp_1 {

	@Test
	void test_1() {

		Response response = get("https://reqres.in/api/users?page=2");
		System.out.println(response.asString());
		System.out.println(response.getBody().asString()); // To return the body and values within as string.
		System.out.println(response.getStatusCode());
		System.out.println(response.getStatusLine());
		System.out.println(response.getHeader("content-type")); // What type of data is returned.
		System.out.println(response.getTime());

		int statusCode = response.getStatusCode();
		Assert.assertEquals(statusCode, 200); // Using assertEquals to check whether ststuCode is 200 or not.
	}

	@Test
	void test_2() {
		// Also to check whether in the given body whether first row of data is equal to
		// 7.
		given().get("https://reqres.in/api/users?page=2").then().statusCode(200).body("data.id[0]", equalTo(7)); // Another
																													// way
																													// to
																													// check
																													// whether
																													// statusCiode
																													// is
																													// 200
																													// or
																													// not.
	}
}
