package RestAssuredBasicOps;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;

import io.restassured.http.ContentType;

import static io.restassured.RestAssured.*;

public class JsonApiTest {
	// @Test
	public void test_get1() {
		// From the base uri retrieving all users.
		baseURI = "http://localhost:3000";
		given().get("/users").then().statusCode(200).log().all();
	}

	// @Test
	public void test_get2() {
		// From the base uri retrieving all subjects.
		baseURI = "http://localhost:3000";
		given().get("/subjects").then().statusCode(200).log().all();
	}

	//@Test
	public void test_get3() {
		// From the base uri retrieving the parameters name as SDET from subjects
		baseURI = "http://localhost:3000";
		given().param("name", "SDET").get("/subjects").then().statusCode(200).log().all();
	}

	// @Test
	public void test_post1() {
		JSONObject request = new JSONObject();
		request.put("firstName", "Tom");
		request.put("lastName", "Holland");
		request.put("subjectId", "1");
		baseURI = "http://localhost:3000";

		given().contentType(ContentType.JSON).accept(ContentType.JSON).header("Content-Type", "application/json")
				.body(request.toJSONString()).when().post("/users").then().statusCode(201).log().all();
	}

	// @Test
	public void test_patch1() {
		JSONObject request = new JSONObject();
		request.put("lastName", "Hardy");
		baseURI = "http://localhost:3000";

		given().contentType(ContentType.JSON).accept(ContentType.JSON).header("Content-Type", "application/json")
				.body(request.toJSONString()).when().patch("/users/4").then().statusCode(200).log().all();
	}

	// @Test
	public void test_put1() {

		JSONObject request = new JSONObject();
		request.put("firstName", "Mary");
		request.put("lastName", "Jane");
		request.put("subjectId", "1");

		baseURI = "http://localhost:3000";

		given().contentType(ContentType.JSON).accept(ContentType.JSON).header("Content-Type", "application/json")
				.body(request.toJSONString()).when().put("/users/4").then().statusCode(200).log().all();
	}

	// @Test
	public void test_delete() {
		baseURI = "http://localhost:3000";
		when().delete("/users/4").then().statusCode(200);
	}

}
