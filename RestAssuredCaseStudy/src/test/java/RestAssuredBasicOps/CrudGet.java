package RestAssuredBasicOps;

import org.testng.annotations.Test;
import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*; //.body working

public class CrudGet {

	@Test
	public void test_1() {
		// GET OPERATION AND CHECK STATUS CODE IS 200 OR NOT.
		given().get("https://reqres.in/api/users?page=2").then().statusCode(200).body("data.id[1]", equalTo(8));
	}

	@Test
	public void test_2() {
		// Checking whether the body has firsnames Micheal and Lindsay.
		given().get("https://reqres.in/api/users?page=2").then().body("data.first_name",
				hasItems("Michael", "Lindsay"));
	}

	@Test
	public void test_3() {
		// Get all the logs/details.
		given().get("https://reqres.in/api/users?page=2").then().statusCode(200).log().all();
	}
}
